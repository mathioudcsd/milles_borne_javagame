package model.card;

public abstract class Card{

	private String name;
	private boolean playable;
	
	
	public Card(){
		this.playable=false;
		this.name="";
	}
	/**
	 * <b>Constructor</b> : Creates a new card, given a name.
	 * @param n The name of the card.
	 */
	public Card(String n){
		this.playable=false;
		this.name=n;
	}
	
	
	/**
	 * Checks if the card in the hand, is playable 
	 * <p><b>Observer:</b></p> Returns if the card in the hand is playable
	 * <b>Postcondition:</b> Boolean value has been returned
	 * @return Boolean value, depending on whether the card is playable or not
	 */
	public boolean isPlayable(){
		return this.playable;
	}
	
	/**
	 * Sets the card to be playable or not
	 * 
	 * <p><b>Transformer: </b></p> Sets the playable value for the card
	 * <p><b>Postconditions:</b></p>The boolean value is set
	 * 
	 * @param v 
	 * The boolean value we are going to set
	 */
	public void setPlayable(boolean v){
		this.playable=v;		
	}
	

	/**
	 * Returns a string representation of the card
	 * <p><b>Postcondition:</b></p>The string representation of a card is returned
	 * @return The string representation of a card 
	 */
	public String toString(){
		return this.name;
	}
}
