package model.card;

public class Distance extends Card {

	private int miles;
	/**
	 * <b>Constructor</b> : Constructs a new card with its parameters
	 */
	public Distance(){
		super();
		this.miles = 0;
	}
	public Distance(String n){
		super(n);
		this.miles=0;
	}
	
	public Distance(int m){
		super("DistanceCard"+m );
		this.miles=m;
	}
	
	public int getMiles(){
		return this.miles;
	}
	
	public String toString(){
		return super.toString();
	}
	
}
