package model.deck;

import java.util.Iterator;

import model.card.Card;
import model.pile.BattlePile;
import model.pile.DiscardPile;
import model.pile.DistancePile;
import model.pile.DrawPile;
import model.pile.HandPile;
import model.pile.Pile;
import model.pile.SafetyPile;
import model.pile.SpeedPile;
//import model.pile.TempPile;

public class Deck {

	private SafetyPile safeP1, safeP2;
	private DistancePile distP1, distP2;
	private SpeedPile speedP1, speedP2;
	private BattlePile battleP1, battleP2;
	//private TempPile tempP1, tempP2;
	private HandPile handP1, handP2;
	private DrawPile drawPile;
	private DiscardPile discPile;
	
	/**
	 * <b>Constructor</b> : Constructs a new deck with its parameters and the piles needed
	 * 
	 * <b>PostConditions</b>: A new deck with its components should be created.
	 */
	public Deck(){
		safeP1= new SafetyPile();
		safeP2= new SafetyPile();
		
		distP1= new DistancePile();
		distP2= new DistancePile();
		
		speedP1= new SpeedPile();
		speedP2= new SpeedPile();
		
		battleP1= new BattlePile();
		battleP2= new BattlePile();
		
		/*
		tempP1= new TempPile();
		tempP2= new TempPile();
		*/
		handP1= new HandPile();
		handP2= new HandPile();
		
		drawPile = new DrawPile();
		
		discPile = new DiscardPile();
		
	}
	
	/**
	 * <b>Accessor</b>: Returns the safety pile.
	 * @param i The player the safety pile belongs to
	 * @return The safety pile of the player
	 */
	public Pile getSafetyPile(int i){
		if(i==1)
			return this.safeP1; 
		else
			return this.safeP2;
	}
	/**
	 * <b>Accessor</b>: Returns the distance pile.
	 * @param i The player the distance pile belongs to
	 * @return The distance pile of the player
	 */
	public Pile getDistancePile(int i){
		if(i==1)
			return this.distP1; 
		else
			return this.distP2;
	}
	/**
	 * <b>Accessor</b>: Returns the Speed pile.
	 * @param i The player the Speed pile belongs to
	 * @return The Speed pile of the player
	 */
	public Pile getSpeedPile(int i){
		if(i==1)
			return this.speedP1; 
		else
			return this.speedP2;
	}
	
	/**
	 * <b>Accessor</b>: Returns the Battle pile.
	 * @param i The player the Battle pile belongs to
	 * @return The Battle pile of the player
	 */
	public Pile getBattlePile(int i){
		if(i==1)
			return this.battleP1; 
		else
			return this.battleP2;
	}
	
	/**
	 * <b>Accessor</b>: Returns the Hand pile.
	 * @param i The player the Hand pile belongs to
	 * @return The Hand pile of the player
	 */
	public Pile getHandPile(int i){
		if(i==1)
			return this.handP1; 
		else
			return this.handP2;
	}
	/**
	 * Returns the draw pile of the deck
	 * @return The draw pile.
	 */
	public Pile getDrawPile(){
		return this.drawPile;
	}
	
	/**
	 * Returns the discard pile of the deck
	 * @return The discard pile.
	 */
	public Pile getDiscPile(){
		return this.discPile;
	}
}
