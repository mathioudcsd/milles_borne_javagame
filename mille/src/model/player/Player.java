package model.player;

import java.util.ArrayList;

import model.card.Card;

public class Player {

	private String name;
	private int points;
	private boolean greenLight,ace,tank,priority,perfectTyre;
	private boolean accident,flatTyre,noGas,limit;
	
	/**
	 * <b>Constructor</b> Creates a player with a name and initializes some parameters.
	 * @param n  the name of the player
	 */
	public Player(String n){
		this.name=n;
		this.points= 0;
		this.greenLight=false;
	}
	
	/**
	 * <b>Accessor</b> Gets the name of the player and returns it.
	 * @return the name of the player.
	 */
	public String getName(){
		return this.name;
	}
	
	/**
	 * <b>Transformer</b> resets all the hazard flags of a  player.
	 * 
	 * <b>PostConditions</b> All the flags should have been reset.
	 */
	public void resetHazardFlags(){
		this.accident=false;
		this.flatTyre=false;
		this.noGas=false;
		this.limit=false;
		this.greenLight=true;
	}
	
	/**
	 * <b>Accessor</b> Gets the points of the player
	 * @return the points
	 */
	public int getPoints(){
		return this.points;
	}
	/**
	 * <b>Transformer</b> Increases the points of the player by i
	 * @param i the integer the points must be increased by this value
	 */
	public void incrPoints(int i){
		this.points=this.points+i;
	}
	
	/**
	 * <b>Transformer</b> Sets the flag greenlight to the b value
	 * @param b a boolean value that the greenlight flag will get
	 */
	public void setGreenLight(boolean b){
		this.greenLight=b;
	}
	
	/**
	 * <b>Transformer</b> Sets the flag ace to the b value
	 * @param b a boolean value that the ace flag will get
	 */
	public void setAce(boolean b){
		this.ace=b;
	}
	
	/**
	 * <b>Transformer</b> Sets the flag tank to the b value
	 * @param b a boolean value that the tank flag will get
	 */
	public void setTank(boolean b){
		this.tank=b;
	}
	
	/**
	 * <b>Transformer</b> Sets the flag priority to the b value
	 * @param b a boolean value that the priority flag will get
	 */
	public void setPriority(boolean b){
		this.priority=b;
	}
	
	/**
	 * <b>Transformer</b> Sets the flag perfectTyre to the b value
	 * @param b a boolean value that the perfectTyre flag will get
	 */
	public void setPerfectTyre(boolean b){
		this.perfectTyre=b;
	}
	
	/**
	 * <b>Observer</b> check if the player has a true/false flatTyre flag
	 * @return a boolean value whether the player has a true/ false flag
	 */
	public boolean isFlatTyre() {
		return flatTyre;
	}


	/**
	 * <b>Observer</b> check if the player has a true/false perfectTyre flag
	 * @return a boolean value whether the player has a true/ false flag
	 */
	public boolean isPerfectTyre() {
		return perfectTyre;
	}

	/**
	 * <b>Observer</b> check if the player has a true/false accident flag
	 * @return a boolean value whether the player has a true/ false flag
	 */
	public boolean isAccident() {
		return accident;
	}

	/**
	 * <b>Observer</b> check if the player has a true/false no Gas flag
	 * @return a boolean value whether the player has a true/ false flag
	 */
	public boolean isNoGas() {
		return noGas;
	}

	/**
	 * <b>Observer</b> check if the player has a true/false limit flag
	 * @return a boolean value whether the player has a true/ false flag
	 */
	public boolean isLimit() {
		return limit;
	}

	/**
	 * <b>Observer</b> check if the player has a true/false greenlight flag
	 * @return a boolean value whether the player has a true/ false flag
	 */
	public boolean hasGreenLight(){
		return this.greenLight;
	}
	
	/**
	 * <b>Observer</b> check if the player has a true/false drivingAce flag
	 * @return a boolean value whether the player has a true/ false flag
	 */
	public boolean isAce(){
		return this.ace;
	}
	
	/**
	 * <b>Observer</b> check if the player has a true/false tank flag
	 * @return a boolean value whether the player has a true/ false flag
	 */
	public boolean hasTank(){
		return this.tank;
	}
	
	/**
	 * <b>Observer</b> check if the player has a true/false fullPriority flag
	 * @return a boolean value whether the player has a true/ false flag
	 */
	public boolean hasPriority(){
		return this.priority;
	}
	
	/**
	 * <b>Transformer</b> Sets the flag accident to the b value
	 * @param b a boolean value that the accident flag will get
	 */
	public void setAccident(boolean b){
		this.accident=b;
	}
	
	/**
	 * <b>Transformer</b> Sets the flag flatTyre to the b value
	 * @param b a boolean value that the flatTyre flag will get
	 */
	public void setFlat(boolean b){
		this.flatTyre=b;
	}
	
	/**
	 * <b>Transformer</b> Sets the flag noGas to the b value
	 * @param b a boolean value that the nogas flag will get
	 */
	public void setNoGas(boolean b){
		this.noGas=b;
	}
	
	/**
	 * <b>Transformer</b> Sets the flag limit to the b value
	 * @param b a boolean value that the limit flag will get
	 */
	public void setLimit(boolean b){
		this.limit=b;
	}
}
