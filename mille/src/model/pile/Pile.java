package model.pile;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import model.card.Card;
import model.card.greenLight;

public class Pile {

	private List<Card> list = new ArrayList<>();
	
	/**
	 * <b>Transformer</b> Adds a card to the pile list.
	 * @param x the card to be added.
	 */
	public void addCard(Card x){
		list.add(x);
	}
	
	/**
	 * <b>Transformer</b> Removes a card from the pile list 
	 * @param x the card to be removed
	 */
	public void removeCard(Card x){
		list.remove(x);
	}
	
	/**
	 * <b>Transformer</b> Removes the first card of the pile list and returns it.
	 * @return The card that was removed
	 */
	public Card removeFirstCard(){
		return list.remove(0);
	}
	
	/**
	 * Returns the list of the pile
	 * @return the list of the pile
	 */
	public List<Card> getList(){
		return this.list;
	}

	/**
	 * <b>Accessor</b>: Prints the list of the pile
	 * 
	 * <b>Postcondition</b> The list of the pile should be printed in the standard output.
	 */
	public void printList(){
		Iterator<Card> itr = list.iterator();
		while(itr.hasNext())
			System.out.print(itr.next()+", ");
	}
	
	/**
	 * <b>Observer</b> Checks if the list of the pile is empty.
	 * @return A boolean value depending on whether the list of the pile is empty.
	 */
	public boolean isEmpty(){
		return list.isEmpty();
	}
	
	/**
	 * <b>Transformer</b> Shuffles the list.
	 */
	public void shuffle(){
		long seed = System.nanoTime();
		Collections.shuffle( this.list, new Random());
	}
}