package model.pile;

import java.util.Iterator;

import model.card.Card;

public class DrawPile extends Pile{
	
	
	/**
	 * Removes a card from the pile
	 * 
	 * @param x The card to be removed
	 */
	public void removeCard(Card x){
		this.getList().remove(x);
	}

	
	

}
