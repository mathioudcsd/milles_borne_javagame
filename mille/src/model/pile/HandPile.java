package model.pile;

import model.card.Card;

public class HandPile extends Pile{
	
	/**
	 * Removes a card from the pile
	 * 
	 * @param x The card to be removed
	 */
	public void removeCard(Card x){
		this.getList().remove(x);
	}
}
