package Controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import javax.swing.ImageIcon;

import View.WinAbsolute;
import model.card.Card;
import model.card.Distance;
import model.card.Hazard;
import model.card.Remedy;
import model.card.Safety;
import model.card.accident;
import model.card.drivingAce;
import model.card.flatTyre;
import model.card.fuelTank;
import model.card.fullPriority;
import model.card.gasRefill;
import model.card.greenLight;
import model.card.noGas;
import model.card.noLimit;
import model.card.perfectTyre;
import model.card.redLight;
import model.card.repairAccident;
import model.card.spareTyre;
import model.card.yesLimit;
import model.deck.Deck;
import model.player.Player;

public class Controller {
	
	private enum Action {Play, Discard};

	private Deck table = new Deck();
	
	private Player player1,player2;
	
	private Map<Integer, Player> players= new HashMap<Integer, Player>();
	
	private int p1points, p2points;
	
	private WinAbsolute window;
	
	/**
	 * <b>Constructor</b>: Constructs a new controller creating the players, initializing the points,
	 * the cards and the icons.
	 * 
	 * <b>PostCondition</b>: Constructs the controller , which will create the game and take care of it. 
	 */
	private Controller(){
		player1 = new Player("player1");
		player2= new Player("player2");
		
		p1points=0;
		p2points=0;
		players.put(1, player1);
		players.put(2, player2);
		
		init_draw_cards();
		shuffle_draw();
		giveCards();
		
		window = new WinAbsolute();
		this.setIcons();
		window.repaint();
	}
	
	/**
	 * <b>Transformer</b>: Gets the player i, with the i key value from the map.
	 * @param i 1 for player 1 , 2 for player 2.
	 * @return The player the i key stands for.
	 */
	private Player getPlayer(int i){
		return this.players.get(i);
	}
	
	/**
	 * <b>Accessor</b>: Initializes the DrawPile , without shuffling.
	 * 
	 * <b>PostConditions</b>: The Draw Pile has been created, without shuffling.
	 */
	private void init_draw_cards(){
		
		for(int i=0; i<10;i++){
			Distance dist25= new Distance(25);
			Distance dist50= new Distance(50);
			Distance dist75= new Distance(75);
			
			this.table.getDrawPile().addCard(dist25);
			this.table.getDrawPile().addCard(dist50);
			this.table.getDrawPile().addCard(dist75);
		}
		
		for(int i=0;i<6;i++){
			repairAccident ra = new repairAccident("repairAccidentCard");
			gasRefill gr = new gasRefill("gasRefillCard");
			spareTyre st = new spareTyre("spareTyreCard");
			noLimit nl = new noLimit("noLimitCard");
			
			this.table.getDrawPile().addCard(ra);
			this.table.getDrawPile().addCard(gr);
			this.table.getDrawPile().addCard(st);
			this.table.getDrawPile().addCard(nl);
		}
		
		for(int i=0; i<3;i++){
			accident ac = new accident("accidentCard");
			noGas ng = new noGas("noGasCard");
			flatTyre ft= new flatTyre("flatTyreCard");
			
			this.table.getDrawPile().addCard(ac);
			this.table.getDrawPile().addCard(ng);
			this.table.getDrawPile().addCard(ft);
		}
		
		for(int i=0; i<4;i++){
			Distance dist200 = new Distance(200);
			yesLimit lc = new yesLimit("yesLimitCard");
			
			this.table.getDrawPile().addCard(dist200);
			this.table.getDrawPile().addCard(lc);
		}
		
		for(int i=0;i<14;i++){
			greenLight gl = new greenLight("greenLightCard");
			this.table.getDrawPile().addCard(gl);
		}
		
		for(int i=0;i<12;i++){
			Distance dist100 = new Distance(100);
			this.table.getDrawPile().addCard(dist100);
		}
		
		for(int i=0; i<5; i++){
			redLight rl = new redLight("redLightCard");
			this.table.getDrawPile().addCard(rl);
		}
			
		drivingAce da =new drivingAce("drivingAceCard");
		fuelTank ftc = new fuelTank("fuelTankCard");
		fullPriority fpc = new fullPriority("fullPriorityCard");
		perfectTyre pt = new perfectTyre("perfectTyreCard");
		
		this.table.getDrawPile().addCard(pt);
		this.table.getDrawPile().addCard(fpc);
		this.table.getDrawPile().addCard(ftc);
		this.table.getDrawPile().addCard(da);
	}
	
	/**
	 * <b>Transformer</b>: Shuffles the draw pile.
	 * 
	 * <b>PostConditions</b>: After the call of this method, the draw pile should be shuffled.
	 */
	private void shuffle_draw(){
		/*long seed = System.nanoTime();
		Collections.shuffle((List<?>) this.table.getDrawPile(),new Random(seed)); */
		this.table.getDrawPile().shuffle();
	}
	
	/**
	 * <b>Accessor</b> Just prints the draw pile list of cards.
	 * 
	 * <b>Postconditions</b>: After the call of this method, the Standard output should have
	 * printed the draw pile.
	 */
	private void show_draw(){
		this.table.getDrawPile().printList();
	}
	
	/**
	 * <b>Transformer</b>: Initializes the hands of the two players, making them
	 * draw 6 cards each.
	 * 
	 * <b>Postcondition</b>: Both hands of the 2 players should be initialized,
	 *  both having drown 6 cards.
	 */
	private void giveCards(){
		for(int i=0;i<6;i++){
			drawCard(1);
			drawCard(2);
		}
	}
	
	/**
	 * <b>Transformer</b>: Adds one card to the hand of the player indicated by the key value.
	 * @param i The key value that indicates the corresponding player in the map.
	 */
	private void drawCard(int i){
		Card x = this.table.getDrawPile().removeFirstCard();
		this.table.getHandPile(i).addCard(x);
		
	}
	
	
	/**
	 * <b>Transformer</b>: Marks a Card playable or not.
	 * @param i The player, to whom the card belongs.
	 * @param a The action the player wants to take with that card.
	 * 
	 * <b>Postcondition</b>: Marks the card playable if the rules say so, or not playable.
	 */
	//can be used as mark_clickable
	private void mark_playable(int i, Action a){
		List<Card> handlist = this.table.getHandPile(i).getList();
		for (int pos = 0; pos<handlist.size(); pos++) {
			if(canPlayCard(i,handlist.get(pos),Action.Play)){
				handlist.get(pos).setPlayable(true);
			}
			else
				handlist.get(pos).setPlayable(false);
		}
	}
	
	/**
	 * <b>Observer</b> Checks if a card can be played. This method is the rules of the game.
	 * @param i The player to whom the card belongs.
	 * @param x The card of which the playability is being checked.
	 * @param a The action that the player wants to take with the card.
	 * @return A boolean value, true or false, whether the card can be played according to the game rules.
	 */
	private boolean canPlayCard(int i, Card x, Action a){
		int enemy;
		if(i%2 !=0)
			enemy=2;
		else
			enemy=1;
		if(a==Action.Discard){
			return true;
		}
		else{
			if(x instanceof Hazard){
				if(this.players.get(enemy).hasGreenLight() || this.players.get(enemy).hasPriority()){
					if(x instanceof accident){
						if(this.players.get(enemy).isAce())
							return false;
						else
							return true;
					}
					
					if(x instanceof flatTyre){
						if(this.players.get(enemy).isPerfectTyre())
							return false;
						else 
							return true;
					}
					
					if(x instanceof noGas){
						if(this.players.get(enemy).hasTank())
							return false;
						else
							return true;
					}
					
					if(x instanceof redLight){
						if(this.players.get(enemy).hasPriority())
							return false;
						else
							return true;
					}
					
					if(x instanceof yesLimit){
						if(this.players.get(enemy).hasPriority())
							return false;
						else
							return true;
					}
					
				}
				else{
					return false;
				}
			}
			
			if(x instanceof Distance){
				if(this.players.get(i).hasGreenLight() || this.players.get(i).hasPriority()){
					if(this.players.get(i).isLimit()==true){
						if(((Distance) x).getMiles()>50 || this.players.get(i).getPoints()+ ((Distance) x).getMiles()>1000)
							return false;
						else 
							return true;
					}
					
					if(this.players.get(i).isFlatTyre()==true){
						return false;
					}
					
					if(this.players.get(i).isAccident()==true){
						return false;
					}
					
					if(this.players.get(i).isNoGas()==true){
						return false;
					}
					return true;
				}
				else
					return false;
				
			}
			
			if(x instanceof Remedy){
				if(x instanceof gasRefill){
					if(this.players.get(i).hasTank()){
						return false;
					}
					else{
						if(this.players.get(i).isNoGas()){
							return true;
						}
						else
							return false;
					}
				}
				
				if(x instanceof greenLight){
					if(this.players.get(i).hasPriority()){
						return false;
					}
					else{
						if(this.players.get(i).hasGreenLight()==false){
							return true;
						}
						else
							return false;
					}
				}
				
				if(x instanceof noLimit){
					if(this.players.get(i).hasPriority()){
						return false;
					}
					else{
						if(this.players.get(i).isLimit()){
							return true;
						}
						else
							return false;
					}
				}
				
				if(x instanceof repairAccident){
					if(this.players.get(i).isAce()){
						return false;
					}
					else{
						if(this.players.get(i).isAccident()){
							return true;
						}
						else
							return false;
					}
				}
				
				if(x instanceof spareTyre){
					if(this.players.get(i).isPerfectTyre()){
						return false;
					}
					else{
						if(this.players.get(i).isFlatTyre()){
							return true;
						}
						else
							return false;
					}
				}
			}
			
			if(x instanceof Safety){
				return true;
			}
		}
		
		return true;
		
	}
	
	/**
	 * <b>Transformer</b>: Gives the buttons the card pictures they are supposed to have in
	 * every phase of the game.
	 * 
	 * <b>Postconditions</b>: The buttons should have the right images on them.
	 */
	private void setIcons(){
		//discardpile
		if(!this.table.getDiscPile().getList().isEmpty()){
			Card x = this.table.getDiscPile().getList().get(table.getDiscPile().getList().size()-1);
			window.getButtonArray().get(29).setIcon(new ImageIcon(window.decideCardImage(x)));
		}
		else{
			window.getButtonArray().get(29).setIcon(new ImageIcon(window.decideCardImage(null)));
		}
		
		window.setLabels(this.getPlayer(1).getPoints(), this.getPlayer(2).getPoints());
		//player1 hand 
		if(this.table.getHandPile(1).getList().size()>6){
			for(int i=0;i<7;i++){
				try{
				Card x= this.table.getHandPile(1).getList().get(i);
				window.getButtonArray().get(i).setIcon(new ImageIcon(window.decideCardImage(x)));
				}catch(Exception ex){}
			}
		}
		else{
			for(int i=0;i<7;i++){
				try{
				Card x=this.table.getHandPile(1).getList().get(i);
				window.getButtonArray().get(i).setIcon(new ImageIcon(window.decideCardImage(x)));
				}catch(Exception ex){}
			}
			window.getButtonArray().get(6).setIcon(new ImageIcon(window.decideCardImage(null)));
		}
		
		//player2 hand
		if(this.table.getHandPile(2).getList().size()>6){
			System.out.println("PANW APO 6 STON 2");
			for(int i=14;i<21;i++){
				try{
				Card x= this.table.getHandPile(2).getList().get(i-14);
				window.getButtonArray().get(i).setIcon(new ImageIcon(window.decideCardImage(x)));
				}catch(Exception ex){}
			}
			window.repaint();
		}
		else{
			System.out.println("<=6 STON 2");
			for(int i=14;i<21;i++){
				try{
				Card x= this.table.getHandPile(2).getList().get(i-14);
				window.getButtonArray().get(i).setIcon(new ImageIcon(window.decideCardImage(x)));
				}catch(Exception ex){}
			}
			window.getButtonArray().get(20).setIcon(new ImageIcon(window.decideCardImage(null)));
		}
		
		//speedpile P1
		if(!this.table.getSpeedPile(1).isEmpty()){
			Card x = this.table.getSpeedPile(1).getList().get(this.table.getSpeedPile(1).getList().size()-1);
			window.getButtonArray().get(7).setIcon(new ImageIcon(window.decideCardImage(x)));
		}
		
		//battlepile P1
		if(!this.table.getBattlePile(1).isEmpty()){
			Card x = this.table.getBattlePile(1).getList().get(this.table.getBattlePile(1).getList().size()-1);
			window.getButtonArray().get(8).setIcon(new ImageIcon(window.decideCardImage(x)));
		}
		
		//distancepile P1
		if(!this.table.getDistancePile(1).isEmpty()){
			Card x =this.table.getDistancePile(1).getList().get(this.table.getDistancePile(1).getList().size()-1);
			window.getButtonArray().get(9).setIcon(new ImageIcon(window.decideCardImage(x)));
		}
		
		//safety P1
		if(this.table.getSafetyPile(1).isEmpty()){
			for(int i=10;i<14;i++){
				try{
					window.getButtonArray().get(i).setIcon(new ImageIcon(window.decideCardImage(null)));
				}catch(Exception ex){}
			}
		}
		else{
			Card x;
			if(this.table.getSafetyPile(1).getList().size()==1){
				x= this.table.getSafetyPile(1).getList().get(this.table.getSafetyPile(1).getList().size()-1);
				window.getButtonArray().get(10).setIcon(new ImageIcon(window.decideCardImage(x)));
			}
			
			if(this.table.getSafetyPile(1).getList().size()==2){
				x= this.table.getSafetyPile(1).getList().get(this.table.getSafetyPile(1).getList().size()-1);
				window.getButtonArray().get(10).setIcon(new ImageIcon(window.decideCardImage(x)));
				
				x= this.table.getSafetyPile(1).getList().get(this.table.getSafetyPile(1).getList().size()-2);
				window.getButtonArray().get(11).setIcon(new ImageIcon(window.decideCardImage(x)));
			}
			
			if(this.table.getSafetyPile(1).getList().size()==3){
				x= this.table.getSafetyPile(1).getList().get(this.table.getSafetyPile(1).getList().size()-1);
				window.getButtonArray().get(10).setIcon(new ImageIcon(window.decideCardImage(x)));
				
				x= this.table.getSafetyPile(1).getList().get(this.table.getSafetyPile(1).getList().size()-2);
				window.getButtonArray().get(11).setIcon(new ImageIcon(window.decideCardImage(x)));
				
				x= this.table.getSafetyPile(1).getList().get(this.table.getSafetyPile(1).getList().size()-3);
				window.getButtonArray().get(12).setIcon(new ImageIcon(window.decideCardImage(x)));
			}
			
			if(this.table.getSafetyPile(1).getList().size()==4){
				x= this.table.getSafetyPile(1).getList().get(this.table.getSafetyPile(1).getList().size()-1);
				window.getButtonArray().get(10).setIcon(new ImageIcon(window.decideCardImage(x)));
				
				x= this.table.getSafetyPile(1).getList().get(this.table.getSafetyPile(1).getList().size()-2);
				window.getButtonArray().get(11).setIcon(new ImageIcon(window.decideCardImage(x)));
				
				x= this.table.getSafetyPile(1).getList().get(this.table.getSafetyPile(1).getList().size()-3);
				window.getButtonArray().get(12).setIcon(new ImageIcon(window.decideCardImage(x)));
				
				x= this.table.getSafetyPile(1).getList().get(this.table.getSafetyPile(1).getList().size()-4);
				window.getButtonArray().get(13).setIcon(new ImageIcon(window.decideCardImage(x)));
			}
		}
		
		
		
		
		
		
		
			//SPEEDPILE P2
			if(!this.table.getSpeedPile(2).isEmpty()){
				Card x = this.table.getSpeedPile(2).getList().get(this.table.getSpeedPile(2).getList().size()-1);
				window.getButtonArray().get(27).setIcon(new ImageIcon(window.decideCardImage(x)));
			}
				
			//battlepile P2
			if(!this.table.getBattlePile(2).isEmpty()){
				Card x = this.table.getBattlePile(2).getList().get(this.table.getBattlePile(2).getList().size()-1);
				window.getButtonArray().get(26).setIcon(new ImageIcon(window.decideCardImage(x)));
			}
				
			//distancepile P2
			if(!this.table.getDistancePile(2).isEmpty()){
				Card x =this.table.getDistancePile(2).getList().get(this.table.getDistancePile(2).getList().size()-1);
				window.getButtonArray().get(25).setIcon(new ImageIcon(window.decideCardImage(x)));
			}
				
				//safety P2
			if(this.table.getSafetyPile(2).isEmpty()){
				for(int i=10;i<14;i++){
					try{
						window.getButtonArray().get(24).setIcon(new ImageIcon(window.decideCardImage(null)));
					}catch(Exception ex){}
				}
			}
			else{
				Card x;
				if(this.table.getSafetyPile(2).getList().size()==1){
					x= this.table.getSafetyPile(2).getList().get(this.table.getSafetyPile(2).getList().size()-1);
					window.getButtonArray().get(24).setIcon(new ImageIcon(window.decideCardImage(x)));
				}
				
				if(this.table.getSafetyPile(2).getList().size()==2){
					x= this.table.getSafetyPile(2).getList().get(this.table.getSafetyPile(2).getList().size()-1);
					window.getButtonArray().get(24).setIcon(new ImageIcon(window.decideCardImage(x)));
					
					x= this.table.getSafetyPile(2).getList().get(this.table.getSafetyPile(2).getList().size()-2);
					window.getButtonArray().get(23).setIcon(new ImageIcon(window.decideCardImage(x)));
				}
					
				if(this.table.getSafetyPile(2).getList().size()==3){
					x= this.table.getSafetyPile(2).getList().get(this.table.getSafetyPile(2).getList().size()-1);
					window.getButtonArray().get(24).setIcon(new ImageIcon(window.decideCardImage(x)));
					
					x= this.table.getSafetyPile(2).getList().get(this.table.getSafetyPile(2).getList().size()-2);
					window.getButtonArray().get(23).setIcon(new ImageIcon(window.decideCardImage(x)));
						
					x= this.table.getSafetyPile(2).getList().get(this.table.getSafetyPile(2).getList().size()-3);
					window.getButtonArray().get(22).setIcon(new ImageIcon(window.decideCardImage(x)));
				}
					
				if(this.table.getSafetyPile(2).getList().size()==4){
					x= this.table.getSafetyPile(2).getList().get(this.table.getSafetyPile(2).getList().size()-1);
					window.getButtonArray().get(24).setIcon(new ImageIcon(window.decideCardImage(x)));
						
					x= this.table.getSafetyPile(2).getList().get(this.table.getSafetyPile(2).getList().size()-2);
					window.getButtonArray().get(23).setIcon(new ImageIcon(window.decideCardImage(x)));
						
					x= this.table.getSafetyPile(2).getList().get(this.table.getSafetyPile(2).getList().size()-3);
					window.getButtonArray().get(22).setIcon(new ImageIcon(window.decideCardImage(x)));
						
					x= this.table.getSafetyPile(2).getList().get(this.table.getSafetyPile(2).getList().size()-4);
					window.getButtonArray().get(21).setIcon(new ImageIcon(window.decideCardImage(x)));
				}
			}
				
	}

	/**
	 * The method that will help the player decide what card is going to be played, and what action
	 * will be taken with this card.
	 * @param i The key value , that indicates what player is playing.
	 * 
	 * <b>Postconditions</b>: By the end of the call of this method, the player currently playing should
	 * have decided what card to play and what to do with it.
	 */
	private void player_plays(int i){
		Card x;
		boolean hasToDrawAgain=false;
		boolean hasFinished=false;
		
		draw_Card(i);
		this.setIcons();
		this.window.repaint();
		
		while(!hasFinished){
			
			if(hasToDrawAgain==true){
				draw_Card(i);
				this.setIcons();
				this.window.repaint();
			}
				
			mark_playable(i, Action.Play);
			x = getCardToPlay2(i);
			System.out.println(x);
			window.resetReaders();
			Action a = getAction();
			window.resetReaders();
			
			boolean canPlay = canPlayCard(i,x,a);
			
			if(canPlay){
				playCard(i,x,a);
				this.setIcons();
				this.window.repaint();
				if(!(x instanceof Safety))
					hasFinished= true;
				else
					
					hasToDrawAgain=true;
			}
		}
		
	}
	
	/**
	 * <b>Transformer</b>: This method manages the cards' destination, after the players have made their moves.
	 * @param i The key referring to the corresponding player in the map.
	 * @param x The card that was played.
	 * @param a The action that was taken.
	 */
	private void playCard(int i, Card x, Action a) {
		int enemy;
		if(i%2 !=0)
			enemy=2;
		else
			enemy=1;
		if(a==Action.Discard){
			this.table.getDiscPile().addCard(x);
			this.table.getHandPile(i).removeCard(x);
		}
		else{
			if(x instanceof Distance){
				players.get(i).incrPoints(((Distance) x).getMiles());
				this.table.getDistancePile(i).addCard(x);
				this.table.getHandPile(i).removeCard(x);
				
				
			}
			
			if(x instanceof Safety){
				if(x instanceof drivingAce){
					players.get(i).setAce(true);
					this.table.getSafetyPile(i).addCard(x);
					this.table.getHandPile(i).removeCard(x);
				}
				
				if(x instanceof fuelTank){
					players.get(i).setTank(true);
					this.table.getSafetyPile(i).addCard(x);
					this.table.getHandPile(i).removeCard(x);
				}
				
				if(x instanceof fullPriority){
					players.get(i).setPriority(true);
					this.table.getSafetyPile(i).addCard(x);
					this.table.getHandPile(i).removeCard(x);
				}
				
				if(x instanceof perfectTyre){
					players.get(i).setPerfectTyre(true);
					this.table.getSafetyPile(i).addCard(x);
					this.table.getHandPile(i).removeCard(x);
				}
			}
			
			if(x instanceof Hazard){
				this.players.get(enemy).resetHazardFlags();
				if(x instanceof accident){
					players.get(enemy).setAccident(true);
					this.table.getBattlePile(enemy).addCard(x);
					this.table.getHandPile(i).removeCard(x);
				}
				
				if(x instanceof flatTyre){
					players.get(enemy).setFlat(true);
					this.table.getBattlePile(enemy).addCard(x);
					this.table.getHandPile(i).removeCard(x);
				}
				
				if(x instanceof noGas){
					players.get(enemy).setNoGas(true);
					this.table.getBattlePile(enemy).addCard(x);
					this.table.getHandPile(i).removeCard(x);
				}
				
				if(x instanceof redLight){
					players.get(enemy).setGreenLight(false);
					this.table.getBattlePile(enemy).addCard(x);
					this.table.getHandPile(i).removeCard(x);
				}
				
				if(x instanceof yesLimit){
					players.get(enemy).setLimit(true);
					this.table.getSpeedPile(enemy).addCard(x);
					this.table.getHandPile(i).removeCard(x);
				}
			}
			
			if(x instanceof Remedy){
				if(x instanceof gasRefill){
					players.get(i).setNoGas(false);
					this.table.getBattlePile(i).addCard(x);
					this.table.getHandPile(i).removeCard(x);
				}
				
				if(x instanceof noLimit){
					players.get(i).setLimit(false);
					this.table.getSpeedPile(i).addCard(x);
					this.table.getHandPile(i).removeCard(x);
				}
				
				if(x instanceof repairAccident){
					players.get(i).setAccident(false);
					this.table.getBattlePile(i).addCard(x);
					this.table.getHandPile(i).removeCard(x);
				}
				
				if(x instanceof spareTyre){
					players.get(i).setFlat(false);
					this.table.getBattlePile(i).addCard(x);
					this.table.getHandPile(i).removeCard(x);
				}
				if(x instanceof greenLight){
					players.get(i).setGreenLight(true);
					this.table.getBattlePile(i).addCard(x);
					this.table.getHandPile(i).removeCard(x);
				}
			}
		}
	}
	
	/**
	 * <b>Transformer</b>: adds a card from the draw pile to the player's hand
	 * @param i The Key value indicating the player on the map.
	 */
	private void draw_Card(int i){
		List<Card> handlist = this.table.getHandPile(i).getList();
		if(!this.table.getDrawPile().isEmpty()){
			Card drawCard = this.table.getDrawPile().removeFirstCard();
			handlist.add(drawCard);
		}
	}
	// testingEND
	
	/**
	 * <b>Accessor</b>: This method helps the player in his turn to play the card.
	 * Waits input from our new input stream.
	 * @param i The key referring to the player that has to play.
	 * @return The card the player chose to play.
	 */
	private Card getCardToPlay2(int i) {
		
		// TODO InputReader from WinAbsolute field
		Scanner keyboard = new Scanner(window.getInputStream());
		
		//display options
		List<Card> handlist = this.table.getHandPile(i).getList();
		System.out.println("Player"+i);
		for (int pos = 0; pos<handlist.size(); pos++) {
			String handCard = pos + ": " + handlist.get(pos).toString()+" "+handlist.get(pos).isPlayable();
			System.out.println(handCard);
		}
		
		//get selection
		System.out.print("Give choice: ");
		while(true) {
			keyboard = new Scanner(window.getInputStream());
			if (keyboard.hasNextInt()) {
				break;
			}
		}
		int choice = keyboard.nextInt();
		System.out.println("Got input!!" + choice);
		keyboard.close();
		return handlist.get(choice - (i-1)*14);

	}
	
	/**
	 * <b>Accessor</b>: Gets the action the player wants to take with a specific card.
	 * @return The action that the player wants to take. Play or discard.
	 */
	private Action getAction(){
		// TODO take input from window
		Scanner input= new Scanner(window.getInputStream());
		System.out.println("98: Play, 99: Discard");
		while(true){
			input= new Scanner(window.getInputStream());
			if(input.hasNextInt()){
				break;
			}
		}
		int choice= input.nextInt();
		System.out.println("Got action!" + choice);
		input.close();
		if(choice==98)
			return Action.Play;
		else
			return Action.Discard;
		
	}

	/**
	 * <b>Observer</b> Checks all the conditions that can lead to the conclusion whether the game is finished or not
	 * @return a boolean value that indicates if the game is finished or not.
	 */
	private boolean isGameFinished(){
		boolean reachMax =this.player1.getPoints()==1000 || this.player2.getPoints()==1000;
		boolean noMoreCards = this.table.getHandPile(1).isEmpty() && this.table.getHandPile(2).isEmpty();
		return (reachMax || noMoreCards);
	}
	
	public static void main(String[] args){
		// initialize Controller
		Controller game= new Controller();
		printDeck(game,1);
		printDeck(game,2);
		
		// keep playing while 
		//		1. hands not empty
		//		2. points < 1000
		
		while(!game.isGameFinished()) {
			//player 1 plays
			game.player_plays(1);
			printDeck(game,1);
			printDeck(game,2);
			//player 2 plays
			game.player_plays(2);
			printDeck(game,1);
			printDeck(game,2);
		}
	}

	public static void printDeck(Controller game, int i) {
		System.out.println("NAME: "+game.getPlayer(i).getName()+" POINTS: "+game.getPlayer(i).getPoints()+" GREENLIGHT: "+game.getPlayer(i).hasGreenLight()+
				" ACE: "+ game.getPlayer(i).isAce()+" TANK: "+ game.getPlayer(i).hasTank()+" PRIORITY: "+game.getPlayer(i).hasPriority()+
				"\nPRFCTTYRE: "+game.getPlayer(i).isPerfectTyre()+" ACCIDENT: "+game.getPlayer(i).isAccident()+ " FLATTYRE: "+ game.getPlayer(i).isFlatTyre()+
				"\nNOGASOLINE: "+game.getPlayer(i).isNoGas()+" LIMIT: "+game.getPlayer(i).isLimit());
		System.out.println();
		System.out.println();
		
	}
}
