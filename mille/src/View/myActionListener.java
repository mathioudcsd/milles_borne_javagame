package View;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;

import javax.swing.SwingUtilities;
import javax.swing.JButton;

public class myActionListener extends InputStream implements MouseListener{
	/**
	 * The Reader
	 */
	StringReader reader = new StringReader("");

	
	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		if(SwingUtilities.isLeftMouseButton(e)){
//			System.out.println(((JButton) e.getSource()).getText());
			this.reader = new StringReader(((JButton) e.getSource()).getText());
		}
		
		if(SwingUtilities.isRightMouseButton(e)){
			System.out.println("RIGHT "+ ((JButton) e.getSource()).getText());
		}
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	/**
	 * REsets The reader.
	 */
	public void resetReader() {
		this.reader= new StringReader("");
		System.out.println("Resetting reader ");
		//reader.close();
	}
	
	@Override
	public int read() throws IOException {		
		return reader.read();
	}

}
