package View;

import java.awt.Color;
import java.io.InputStream;
import java.io.SequenceInputStream;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

import model.card.Card;
import model.card.Distance;
import model.card.accident;
import model.card.drivingAce;
import model.card.flatTyre;
import model.card.fuelTank;
import model.card.fullPriority;
import model.card.gasRefill;
import model.card.greenLight;
import model.card.noGas;
import model.card.noLimit;
import model.card.perfectTyre;
import model.card.redLight;
import model.card.repairAccident;
import model.card.spareTyre;
import model.card.yesLimit;

public class WinAbsolute extends JFrame {

	private static final int WIDTH = 1024;
	private static final int HEIGHT = 1024;
	private static final int CARD_WIDTH = 98;
	private static final int CARD_HEIGHT = 140;
	private JLabel points1,points2;
	ArrayList<String> imgArray = new ArrayList<>();
	ArrayList<JButton> buttArray = new ArrayList();
	Vector<myActionListener> lstArray = new Vector<myActionListener>();
/**
 * <b>Constructor</b>: Constructs a new window calling the needed methods to create 
 * buttons and fields.
 * 
 * <b>Postcondition</b>: Constructs a new window containing all the Buttons needed, the text fields,
 * the color, and the action listeners
 */
	public WinAbsolute() {
		super("hello");
		setNameArray();
		// creating main window
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setSize(WIDTH, HEIGHT);
		this.getContentPane().setLayout(null);
		this.getContentPane().setBackground(Color.GREEN);

		// creating buttons & point fields
		points1= new JLabel();
		points2= new JLabel();
		for (int i = 0; i < 30; i++) {
			buttArray.add(new JButton("" + i));
		}
		buttArray.add(new JButton(""+98));  //30
		buttArray.add(new JButton(""+99));  //31
		
		// creating action listeners
		for (int i = 0; i < 16; i++) {
			lstArray.add(new myActionListener());
		}
		
		place_Card_buttons();
		addListeners();
		putImg();
		this.setVisible(true);
	}

	/**
	 * <b>Transformer</b>: Adds the names of the images used in an array list.
	 */
	public void setNameArray(){
		imgArray.add("ACCIDENT.jpg");
		imgArray.add("DRIVING_ACE.jpg");
		imgArray.add("END_OF_LIMIT.jpg");
		imgArray.add("EXTRA_TANK.jpg");
		imgArray.add("FLAT_TIRE.jpg");
		imgArray.add("GASOLINE.jpg");
		imgArray.add("Miles_100.jpg");
		imgArray.add("Miles_200.jpg");
		imgArray.add("Miles_25.jpg");
		imgArray.add("Miles_50.jpg");
		imgArray.add("Miles_75.jpg");
		imgArray.add("OUT_OF_GAS.jpg");
		imgArray.add("PUNCTURE_PROOF.jpg");
		imgArray.add("REPAIR.jpg");
		imgArray.add("RIGHT_OF_WAY.jpg");
		imgArray.add("ROLL.jpg");
		imgArray.add("SPARE_TIRE.jpg");
		imgArray.add("SPEED_LIMIT.jpg");
		imgArray.add("STOP.jpg");
	}
	
	/**
	 * <b>Accessor</b>: Decides which image will be used for each card on the field.
	 * This is happening by checking the instance of the Card x, and getting the 
	 * appropriate string as a return value , from the String array list.
	 * @param x The card for which has to be decided what image will represent it
	 * @return The String that indicates to the location of the image, that will represent the card.
	 */
	public String decideCardImage(Card x){
		if(x instanceof accident)
			return "src/images/"+imgArray.get(0);
		if(x instanceof drivingAce)
			return "src/images/"+imgArray.get(1);
		if(x instanceof noLimit)
			return "src/images/"+imgArray.get(2);
		if(x instanceof fuelTank)
			return "src/images/"+imgArray.get(3);
		if(x instanceof flatTyre)
			return "src/images/"+imgArray.get(4);
		if(x instanceof gasRefill)
			return "src/images/"+imgArray.get(5);
		if(x instanceof Distance){
			if(((Distance) x).getMiles()==100)
				return "src/images/"+imgArray.get(6);
			if(((Distance) x).getMiles()==200)
				return "src/images/"+imgArray.get(7);
			if(((Distance) x).getMiles()==25)
				return "src/images/"+imgArray.get(8);
			if(((Distance) x).getMiles()==50)
				return "src/images/"+imgArray.get(9);
			if(((Distance) x).getMiles()==75)
				return "src/images/"+imgArray.get(10);
		}
		if(x instanceof noGas)
			return "src/images/"+imgArray.get(11);
		if(x instanceof perfectTyre)
			return "src/images/"+imgArray.get(12);
		if(x instanceof repairAccident)
			return "src/images/"+imgArray.get(13);
		if(x instanceof fullPriority)
			return "src/images/"+imgArray.get(14);
		if(x instanceof greenLight)
			return "src/images/"+imgArray.get(15);
		if(x instanceof spareTyre)
			return "src/images/"+imgArray.get(16);
		if(x instanceof yesLimit)
			return "src/images/"+imgArray.get(17);
		if(x instanceof redLight)
			return "src/images/"+imgArray.get(18);
		return null;
		
	}
	
	/**
	 * <b>Accesseor</b>: Caring for the Play, discard and Draw button fields.
	 */
	public void putImg(){
		this.buttArray.get(28).setIcon(new ImageIcon("src/images/me2.jpg"));
		this.buttArray.get(30).setIcon(new ImageIcon("src/images/PLAY.jpg"));
		this.buttArray.get(31).setIcon(new ImageIcon("src/images/discard.jpg"));
		validate();
	}
	
	/**
	 * Returns what has been clicked from the graphicUI.
	 * @return Input from buttons
	 */
	public InputStream getInputStream() {
		// TODO Create input stream - getter
		return  new SequenceInputStream(lstArray.elements());
	}
	
	/**
	 * Resets the Readers of the Input stream of the buttons.
	 */
	public void resetReaders() {
		// TODO
		int i=0;
		
		for( i=0;i<16;i++){	
			lstArray.get(i).resetReader();
			}
	}

	/**
	 * <b>Transformer</b>: Places initial buttons on their positions, without any images attached
	 */
	private void place_Card_buttons() {
		int init = 180, init2 = 130, push = 0;

		for (int i = 0; i < 7; i++) {
			this.getContentPane().add(buttArray.get(i));
			buttArray.get(i)
					.setBounds(init + push, 30, CARD_WIDTH, CARD_HEIGHT);
			push = push + 85;
			this.getContentPane().add(buttArray.get(i));
		}
		push = 0;
		for (int i = 7; i < 14; i++) {
			this.getContentPane().add(buttArray.get(i));
			buttArray.get(i).setBounds(init2 + push, 175, CARD_WIDTH,
					CARD_HEIGHT);
			push = push + 102;
			this.getContentPane().add(buttArray.get(i));
		}

		this.getContentPane().add(buttArray.get(28));
		buttArray.get(28).setBounds(410, 420, CARD_WIDTH, CARD_HEIGHT);
		this.getContentPane().add(buttArray.get(29));
		buttArray.get(29).setBounds(515, 420, CARD_WIDTH, CARD_HEIGHT);
		push = 0;
		this.getContentPane().add(buttArray.get(30));
		buttArray.get(30).setBounds(655, 440, 80, 80);
		this.getContentPane().add(buttArray.get(31));
		buttArray.get(31).setBounds(755, 440, 80, 80);

		for (int i = 14; i < 21; i++) {
			this.getContentPane().add(buttArray.get(i));
			buttArray.get(i).setBounds(init + push, 814, CARD_WIDTH,
					CARD_HEIGHT);
			push = push + 85;
			this.getContentPane().add(buttArray.get(i));
		}
		push = 0;
		for (int i = 21; i < 28; i++) {
			this.getContentPane().add(buttArray.get(i));
			buttArray.get(i).setBounds(init2 + push, 669, CARD_WIDTH,
					CARD_HEIGHT);
			push = push + 102;
			this.getContentPane().add(buttArray.get(i));
		}
		
		this.getContentPane().add(points1);
		points1.setBounds(210, 440, 100, 50);
		
		this.getContentPane().add(points2);
		points2.setBounds(210, 500, 100, 50);
	}

	/**
	 * <b>Transformer</b>: Adds listeners to the buttons that are involved in the gameplay.
	 */
	private void addListeners() {
		for (int i = 0; i < 7; i++) {
			buttArray.get(i).addMouseListener(lstArray.get(i));
		}

		for (int i = 14; i < 21; i++) {
			buttArray.get(i).addMouseListener(lstArray.get(i-7));
		}
		//play-discard
		buttArray.get(30).addMouseListener(lstArray.get(14));
		buttArray.get(31).addMouseListener(lstArray.get(15));
	}

	/**
	 * <b>Accessor</b> Returns the button Array. 
	 * @return The button array of the window.
	 */
	public ArrayList<JButton> getButtonArray(){
		return this.buttArray;
	}
	/**
	 * <b>Accessor</b> Sets the Jtext fields of the points of both players, with every repaint.
	 * @param a Player1 points
	 * @param b Player2 points
	 */
	public void setLabels(int a, int b){
		points1.setText(""+a);
		points2.setText(""+b);
	}
}
